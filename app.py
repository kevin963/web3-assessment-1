from flask import Flask, render_template, url_for, request
from mongoengine import *
import os, csv, json, requests
from flask_cors import CORS

# create and connect mongo Database
connect('web3')

# create document(same means table), declear two field
class Country(Document):
    name = StringField()
    data = DictField()

app = Flask(__name__)
CORS(app)
app.config.from_object('config')

# all route point to main page and return 200 status code
@app.route('/')
@app.route('/index')
@app.route('/home')
def index():
    index = "Index"
    return render_template('index.html', title=index), 200

# An API, when I access this API will got Json type data
@app.route('/countries', methods=['GET','POST'])
@app.route('/countries/<CountryName>', methods=['GET'])
def countries(name = None):
    countries=None
    if name is None:
        countries = Country.objects
    else:
        countries = Country.objects.get(name = name)
    return countries.to_json(),200

# Delete API, cache the countryname then delete it
@app.route('/deleteCountry', methods=['DELETE'])
def removeCountryByName():
    country_name = request.get_json()['name']
    delete_country = Country.objects(name=country_name)
    delete_country.delete()
    return "Deleted country " + country_name, 200

# inspiration page
@app.route('/inspire')
def inspire():
    return render_template('inspire.html') , 200   

# Delete all data
@app.route('/drop')
def drop():
	country = Country.objects
	country.delete()
	return render_template('index.html') ,200

# Create data, by read those three Csv files
@app.route('/loaddata')
def loaddata():
    #Country.objects.delete()
    for file in os.listdir(app.config['FILES_FOLDER']):
        filename = os.fsdecode(file)
        path = os.path.join(app.config['FILES_FOLDER'],filename)
        f = open(path)
        r = csv.DictReader(f) 
        d = list(r)
        for data in d:
            country = Country() # a blank placeholder country
            lib = {} # a blank placeholder data dict
            for key in data: # iterate through the header keys
                if key == "country":
                    if Country.objects(name = data[key]).count() == 0:
                        country['name'] = data[key]                        
                    else:
                        country = Country.objects.get(name = data[key])  
                        lib = country['data']           
                else:
                    f = filename.replace(".csv","") # we want to trim off the ".csv" as we can't save anything with a "." as a mongodb field name
                    if f in lib: # check if this filename is already a field in the dict
                        lib[f][key] = data[key] # if it is, just add a new subfield which is key : data[key] (value)
                    else:
                        lib[f] = {key:data[key]} # if it is not, create a new object and assign it to the dict
                country['data'] = lib
            country.save()   
    return render_template('index.html', title=index), 200

if __name__ == '__main__':
    app.run(host='0.0.0.0', debug=True, port=80)


