# Web3-Assessment-1

## API documentation

### Click API name to fold detail

<details><summary>API GET</summary>
<p>

#### List countries

`GET /countries`
- Will show the all countries 

`GET /countries/<countryName>`
- Will show the specific country

#### Parameters

##### countryName
- used to specify a country name

### Example usage

- GET /countries

returns


    { 
    
        countryName: "Japan", 
        year: "1999",
        Power consumption per capita: "2.5", 
        Life expectancy: "70" 
    },
    {
        countryName: "Italy",
        year: "1999",
        Power consumption per capita: "2.9", 
        Life expectancy: "73" 
    }


- GET /countries/China

    returns


{ 

        countryName: "China",
        year: "1999",
        Power consumption per capita: "1.6", 
        Life expectancy: "69" 
        
}


### Errors

- Resource not found error - 404 Not Found

- GET /countries/Littlepony

    returns


{

    "type": "error",
    "status": 404,
    "code": "not_found",
    "message":"Resource not found. Please ensure resource exists."
    
}

</p>
</details>

<details><summary>DELETE GET</summary>
<p>

#### Delete country


    @app.route("/deleteCountry", methods=["DELETE"])

    def removeCountryByName():

    country_name = request.get_json()["name"]
    
    delete_country = Country.objects(name=country_name)
    
    delete_country.delete()
    
    return "Deleted country " + country_name, 200
 

#### Parameters

##### country_Name
- used to specify a country name

### Example usage

- DELETE /deleteCountry

- if country_Name is Japan

returns

Deleted country Japan


### Errors

You only can delete the country from the CountryList, so there will no error.
</p>
</details>